//
//  Menu.swift
//  MealsTutorial
//
//  Created by Woessner, Philipp on 27.11.19.
//  Copyright © 2019 sap. All rights reserved.
//

import Foundation

struct MenuItem: Codable {
    let allergens: [String]?
    let calories: Int
    let carbohydrate, cost, fat: Double
    let id: Int
    let imageName: String
    let protein: Double
    let spiciness: String?
    let station: String
}

typealias Menu = [String: MenuItem]

struct QueueItem: Codable {
    var customer: String
    var food: String
    var order_time: String
}

typealias Queue = [String: QueueItem]


class DinerBackend {
    static private let baseUrl = "http://10.127.4.109:5000"
    
    // MARK: - Get stations
    public static func getStations(completionHandler: @escaping (_: String?) -> Void) {
        
        if let url = URL(string: baseUrl + "/stations") {
            
            
            
            
            var getRequest = URLRequest(url: url)
            getRequest.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: getRequest) { data, response, error in
                guard data != nil && error == nil else {
                    completionHandler(nil)
                    return
                }
                
                let station = String(data: data!, encoding: .utf8)
                completionHandler(station)
                
            }
            
            task.resume()
        }
    }
    
    // MARK: - Get menu
    public static func getMenu(completionHandler: @escaping (_: Menu?) -> Void) {
        
        if let url = URL(string: baseUrl + "/menu") {
            var getRequest = URLRequest(url: url)
            getRequest.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: getRequest) { data, response, error in
                guard data != nil && error == nil else {
                    completionHandler(nil)
                    return
                }
                
                do {
                    let menu = try JSONDecoder().decode(Menu.self, from: data!)
                    completionHandler(menu)
                } catch {
                    completionHandler(nil)
                }
            }
            
            task.resume()
        }
    }
    
    // MARK: - Get food rating
    public static func getFoodRating(foodName: String, completionHandler: @escaping (_:Double?) -> Void) {
        
        var urlComponent = URLComponents(string: baseUrl + "/rating")
        urlComponent?.queryItems = [URLQueryItem(name: "food", value: foodName)]
        
        if let url = urlComponent?.url {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard data != nil && error == nil else {
                    completionHandler(nil)
                    return
                }
                
                do {
                    let rating = try JSONDecoder().decode(Double.self, from: data!)
                    completionHandler(rating)
                } catch {
                    completionHandler(nil)
                }
            }
            
            task.resume()
        }
    }
    
    // MARK: - Rate food
    public static func rateFood(foodName: String, rating: Int, completionHandler: @escaping (_:Bool?) -> Void) {
        
        guard (1...5).contains(rating) else {
            print("Rating should be a value between 1 to 5")
            return
        }
        
        var urlComponent = URLComponents(string: baseUrl + "/put_rating")
        let foodQueryItem = URLQueryItem(name: "food", value: foodName)
        let ratingQueryItem = URLQueryItem(name: "rating", value: String(rating))
        urlComponent?.queryItems = [foodQueryItem, ratingQueryItem]
        
        if let url = urlComponent?.url {
            
            var request = URLRequest(url: url)
            request.httpMethod = "PUT"
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard data != nil && error == nil else {
                    completionHandler(nil)
                    return
                }
                
                completionHandler(true)
            }
            
            task.resume()
        }
    }
    
    // MARK: - Get queue for station
    public static func getQueueForStation(foodName: String, completionHandler: @escaping (_:Queue?) -> Void) {
        
        var urlComponent = URLComponents(string: baseUrl + "/station_queue")
        urlComponent?.queryItems = [URLQueryItem(name: "station", value: foodName)]
        
        if let url = urlComponent?.url {
            
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard data != nil && error == nil else {
                    completionHandler(nil)
                    return
                }
                
                do {
                    let queue = try JSONDecoder().decode(Queue.self, from: data!)
                    completionHandler(queue)
                } catch {
                    completionHandler(nil)
                }
            }
            
            task.resume()
        }
    }
    
    // MARK: - Put food in queue
    public static func putFoodInQueue(foodName: String, customerName: String, completionHandler: @escaping (_:Bool?) -> Void) {
        
        var urlComponent = URLComponents(string: baseUrl + "/put_food_queue ?food=Pepperoni%20Pizza&customer=Carl")
        let foodQueryItem = URLQueryItem(name: "food", value: foodName)
        let customerQueryItem = URLQueryItem(name: "customer", value: customerName)
        urlComponent?.queryItems = [foodQueryItem, customerQueryItem]
        
        if let url = urlComponent?.url {
            
            var request = URLRequest(url: url)
            request.httpMethod = "PUT"
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard data != nil && error == nil else {
                    completionHandler(nil)
                    return
                }
                
                completionHandler(true)
            }
            
            task.resume()
        }
    }
}
