//
//  DinerBackendCalls.swift
//  MealsTutorial
//
//  Created by Woessner, Philipp on 27.11.19.
//  Copyright © 2019 sap. All rights reserved.
//

import Foundation

class DinerBackendCalls {
    static func callAll() {
        
        
        
        DinerBackend.getStations { station in
            print(station ?? "No value fetched from backend")
        }
        
        
        
        
        
        
        
        
        
        DinerBackend.getMenu { menu in
            print(menu ?? "No value fetched from backend")
        }
        
        
        
        DinerBackend.getFoodRating(foodName: "Margherita Pizza") { rating in
            print(rating ?? "No value fetched from backend")
        }
        
        DinerBackend.rateFood(foodName: "Margherita Pizza", rating: 3) { success in
            print(success ?? "No value fetched from backend")
        }
        
        DinerBackend.getQueueForStation(foodName: "Margherita Pizza") { queue in
            print(queue ?? "No value fetched from backend")
        }
        
        DinerBackend.putFoodInQueue(foodName: "Margherita Pizza", customerName: "Philipp") { success in
            print(success ?? "No value fetched from backend")
        }
    }
}
