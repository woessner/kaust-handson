//
//  ViewController.swift
//  MealsTutorial
//
//  Created by Woessner, Philipp on 27.11.19.
//  Copyright © 2019 sap. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    var selectedMenu: MenuItem!
     
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func userDidSubmit(_ sender: Any) {
        label.text = textField.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    
}

